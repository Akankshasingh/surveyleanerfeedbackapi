package com.test.demo.utils;


import lombok.experimental.UtilityClass;

import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

@UtilityClass
public class CommonUtils {

  public Properties readPropertiesFile() throws IOException {
    Properties prop;
    try(FileInputStream fis = new FileInputStream("application.properties")){
      prop = new Properties();
      prop.load(fis);
    }
    return prop;
  }
}
