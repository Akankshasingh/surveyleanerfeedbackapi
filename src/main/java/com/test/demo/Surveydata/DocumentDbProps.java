package com.test.demo.Surveydata;

import com.test.demo.utils.CommonUtils;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Properties;

@Getter
@Setter
@Component
public class DocumentDbProps {

    @Value("${mongodb.host}")
    private  String dbHost;
    @Value("${mongodb.port}")
    private  int dbPort;
    @Value("${mongodb.database.name}")
    private  String dbName;
    @Value("${mongodb.username}")
    private  String dbUser;
    @Value("${mongodb.password}")
    private  String dbPassword;



}
