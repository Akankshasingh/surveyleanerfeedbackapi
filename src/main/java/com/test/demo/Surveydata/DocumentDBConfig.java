package com.test.demo.Surveydata;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import lombok.extern.slf4j.Slf4j;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import static com.mongodb.client.model.Filters.eq;

@Configuration
@Slf4j
public class DocumentDBConfig {

    @Autowired
    private  DocumentDbProps dbProps;


    public MongoDatabase  mongoDbConfig(){
        String clientUrl = "mongodb://mursion:" + dbProps.getDbPassword() + "@"
                + dbProps.getDbUser() + "-documentdb.czzp0eicsnni.eu-central-1.docdb.amazonaws.com:"
                + dbProps.getDbPort() ;
        MongoClient mongoClient = MongoClients.create(clientUrl);
        MongoDatabase database = mongoClient.getDatabase(dbProps.getDbName()); //SurveyData

        return database;
    }




}
