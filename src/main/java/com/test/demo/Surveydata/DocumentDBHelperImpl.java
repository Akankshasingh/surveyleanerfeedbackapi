package com.test.demo.Surveydata;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.test.demo.Surveydata.dto.DocumentDBFeedbackDTO;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import static com.mongodb.client.model.Filters.eq;

@Repository
public class DocumentDBHelperImpl  implements  DocumentDBHelper {

    @Autowired
    private DocumentDBConfig documentDBConfig;

    public Integer getCollectionData(String tableName, String surveySessionId, String columnName){
        MongoDatabase mongoDatabase = documentDBConfig.mongoDbConfig();
        MongoCollection<Document> mongoCollection = mongoDatabase.getCollection(tableName);
        Document preSimulation = mongoCollection.find(eq("mursionSessionId", surveySessionId)).first();
        return (preSimulation.get(columnName).toString() != null) ? Integer.valueOf(preSimulation.get(columnName).toString()) : null;

    }

    @Override
    public void saveFeedbackData(DocumentDBFeedbackDTO documentDBFeedbackDTO) {
        MongoDatabase mongoDatabase = documentDBConfig.mongoDbConfig();
        MongoCollection<Document> mongoCollection= mongoDatabase.getCollection("learnerFeedbackData");
        Document document = new Document();
        document.append("sessionID", documentDBFeedbackDTO.getSessionID());
        document.append("preSimLearnerConfidenceLevel", documentDBFeedbackDTO.getPreSimLearnerConfidenceLevel());
        document.append("postSimLearnerConfidenceLevel",documentDBFeedbackDTO.getPostSimLearnerConfidenceLevel());
        document.append("learnerConfidenceFeedback", documentDBFeedbackDTO.getLearnerConfidenceFeedback());
        mongoCollection.insertOne(document);

    }


}
