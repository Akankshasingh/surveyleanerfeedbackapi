package com.test.demo.Surveydata;

import com.test.demo.Surveydata.dto.DocumentDBFeedbackDTO;
import com.test.demo.Surveydata.dto.LearnerConfidenceLevel;
import com.test.demo.Surveydata.dto.LearnerFeedbckDTO;
import com.test.demo.Surveydata.dto.LearnerPerspectiveDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static com.mongodb.client.model.Filters.eq;

@Slf4j
@Service
public class LearnerFeedbackServiceImpl implements  LearnerFeedbackService {

    @Autowired
    private DocumentDBConfig documentDBConfig;

    @Autowired
    private DocumentDBHelper documentDBHelper;


    public LearnerPerspectiveDTO learnerConfidencefeedback(LearnerFeedbckDTO learnerFeedbckDTO) {
        log.info("----in the learnerConfidencePerspective-----");
        Integer preSimLearnerConfidenceLevel ;
        Integer postSimLearnerConfidenceLevel ;
        String surveySessionId ;
        String learnerFeedback ;
        DocumentDBFeedbackDTO documentDBFeedbackDTO=new DocumentDBFeedbackDTO();
        LearnerPerspectiveDTO learnerPerspectiveDTO = new LearnerPerspectiveDTO();
        surveySessionId = learnerFeedbckDTO.getSessionId();
        preSimLearnerConfidenceLevel = documentDBHelper.getCollectionData("preSimCollection", surveySessionId, "Q1");
        postSimLearnerConfidenceLevel = documentDBHelper.getCollectionData("postSimCollection", surveySessionId, "Q19");
        log.info("--preSimLearnerConfidenceLvl::---" + preSimLearnerConfidenceLevel);
        if (preSimLearnerConfidenceLevel != null && postSimLearnerConfidenceLevel != null) {
            String preConfidenceLevel = learnerConfidencLevel(preSimLearnerConfidenceLevel);
            String postConidenceLevel = learnerConfidencLevel(postSimLearnerConfidenceLevel);
            learnerFeedback= "Your confidence going into the simulation was " + preConfidenceLevel +
                    " and after the simulation was " + postConidenceLevel ;
            learnerPerspectiveDTO.setLearnerFeedback(learnerFeedback);
            documentDBFeedbackDTO.setSessionID(surveySessionId);
            documentDBFeedbackDTO.setPreSimLearnerConfidenceLevel(preConfidenceLevel);
            documentDBFeedbackDTO.setPostSimLearnerConfidenceLevel(postConidenceLevel);
            documentDBFeedbackDTO.setLearnerConfidenceFeedback(learnerFeedback);
            documentDBHelper.saveFeedbackData(documentDBFeedbackDTO);
        }
        return learnerPerspectiveDTO;
    }

    public String learnerConfidencLevel(Integer level) {
        String confidenceLevel = "";
        switch (level) {
            case 1:
                confidenceLevel = LearnerConfidenceLevel.VERY_LOW.toString();
                break;
            case 2:
                confidenceLevel = LearnerConfidenceLevel.LOW.toString();
                break;
            case 3:
                confidenceLevel = LearnerConfidenceLevel.MODERATE.toString();
                break;
            case 4:
                confidenceLevel = LearnerConfidenceLevel.HIGH.toString();
                break;
            case 5:
                confidenceLevel = LearnerConfidenceLevel.VERY_HIGH.toString();
                break;

        }
        return confidenceLevel;

    }

}
