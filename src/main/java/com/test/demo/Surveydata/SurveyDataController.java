package com.test.demo.Surveydata;

import com.test.demo.Surveydata.dto.LearnerFeedbckDTO;
import com.test.demo.Surveydata.dto.LearnerPerspectiveDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
public class SurveyDataController {
    @Autowired
   private LearnerFeedbackServiceImpl learnerFeedbackServiceImpl;

   @GetMapping(value= "/getLearnerConfidencFeedbck")
   public LearnerPerspectiveDTO learnerfeedback(@RequestBody LearnerFeedbckDTO learnerFeedbckDTO){
     return   learnerFeedbackServiceImpl.learnerConfidencefeedback(learnerFeedbckDTO);
   }

}
