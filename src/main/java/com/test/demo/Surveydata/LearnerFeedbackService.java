package com.test.demo.Surveydata;

import com.test.demo.Surveydata.dto.LearnerFeedbckDTO;
import com.test.demo.Surveydata.dto.LearnerPerspectiveDTO;

public interface LearnerFeedbackService {

    public LearnerPerspectiveDTO learnerConfidencefeedback(LearnerFeedbckDTO learnerFeedbckDTO) ;
}
