package com.test.demo.Surveydata.dto;

public enum LearnerConfidenceLevel {
    VERY_LOW,
    LOW,
    MODERATE,
    HIGH,
    VERY_HIGH
}
