package com.test.demo.Surveydata.dto;

import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

@Setter
@Getter
@Component
public class LearnerPerspectiveDTO {

    private String learnerFeedback;

}
