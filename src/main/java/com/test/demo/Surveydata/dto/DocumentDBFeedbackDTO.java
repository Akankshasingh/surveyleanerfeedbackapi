package com.test.demo.Surveydata.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.stereotype.Component;

@Setter
@Getter
@NoArgsConstructor
@Component
public class DocumentDBFeedbackDTO {
    private String sessionID;
    private String preSimLearnerConfidenceLevel ;
    private String postSimLearnerConfidenceLevel ;
    private String learnerConfidenceFeedback;

}
