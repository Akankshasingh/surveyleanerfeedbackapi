package com.test.demo.Surveydata;


import com.test.demo.Surveydata.dto.DocumentDBFeedbackDTO;
import org.springframework.stereotype.Repository;

@Repository
public interface DocumentDBHelper {

    public Integer getCollectionData(String tableName, String surveySessionId, String columnName);
    public void saveFeedbackData(DocumentDBFeedbackDTO documentDBFeedbackDTO);
}
