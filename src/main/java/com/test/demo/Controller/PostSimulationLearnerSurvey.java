package com.test.demo.Controller;

import lombok.Data;

import java.io.Serializable;

@Data
public class PostSimulationLearnerSurvey extends SurveyDataBaseDTO implements Serializable {

  private String softwareVersion;
  private String preSimulationSurvey;
  private String qLanguage;
  private String hostName;
  private String sessionId;

}
