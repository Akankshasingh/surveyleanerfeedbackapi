package com.test.demo.Controller;

import lombok.Data;

import java.io.Serializable;
import java.util.Map;

@Data
public class SimFeedbackStandardized extends SurveyDataBaseDTO implements Serializable {

  private String clientId;
  private String mursionSessionId;
  private Map<String,String> learnerNameIdMap;
  private String deliveryMode;
  private int sessionAttendanceCount;

}
