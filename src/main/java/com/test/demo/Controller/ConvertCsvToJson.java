package com.test.demo.Controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;
import com.opencsv.exceptions.CsvException;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.ss.util.cellwalk.CellHandler;
import org.apache.poi.ss.util.cellwalk.CellWalk;
import org.apache.poi.ss.util.cellwalk.CellWalkContext;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFTable;
import org.bson.Document;
import org.codehaus.jettison.json.JSONException;
import org.json.CDL;
import org.json.JSONObject;
import org.json.XML;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTTableColumn;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTTableColumns;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import java.io.*;
import java.util.*;

import static com.mongodb.client.model.Filters.eq;

@RestController
public class ConvertCsvToJson {


  @GetMapping("/testMongoDB")
  public String testMongoDB(){

      MongoClient client = MongoClients.create("mongodb://localhost:27017");
      MongoDatabase database = client.getDatabase("SurveyData");
      MongoCollection<Document> collection1 = database.getCollection("Test");
      Document preSimulation = collection1.find(eq("mursionSessionId", "4eb12057-f0f3-4238-a0d6-0a58ac17665e")).first();
      String testOutput = preSimulation.get("projectId").toString();

      return "ok "+ testOutput ;
  }





  @GetMapping("/convertCsvToJson")
    public String ConvertPreSimCsvToJson() throws IOException, InvalidFormatException, org.apache.poi.openxml4j.exceptions.InvalidFormatException, JSONException, CsvException {
      //replaceHeaderToCSv();

      String filepath = "D:/Issues/test1.csv";
      updateCSV(filepath);
        Integer INDENTATION = 2;
        String csvAsString=null;

      File initialFile = new File(filepath);
      InputStream is = new FileInputStream(initialFile);
      System.out.println("inputStream:: "+ is) ;
      String csv = new BufferedReader(
              new InputStreamReader(is, StandardCharsets.UTF_8))
              .lines()
              .collect(Collectors.joining("\n"));

      System.out.println("csv:: "+ csv) ;
        String json = CDL.toJSONArray(csv).toString();

        Files.write(Path.of("D:/Issues/testJson.json"), json.getBytes());

        return "k" ;
    }

    public static void updateCSV(String fileToUpdate) throws IOException, CsvException {

        File inputFile = new File(fileToUpdate);
        List<String> dtoHeaders =  Arrays.asList( "startDate"	,"endDate"	,"status"	,"ipAddress"	,"progress",	"durationSeconds"	,"finished",	"recordedDate",	"responseId"	,"recipientLastName"	,"recipientFirstName",	"recipientEmail",	"externalReference",	"locationLatitude",	"locationLongitude",	"distributionChannel"	,"userLanguage"	,"Q19"	,"Q20"	,"Q21"	,"Q22"	,"Q23"	,"Q26"	,"Q27"	,"Q25",	"Q24_1"	,"Q24_2"	,"Q24_3"	,"Q24_4"	,"Q24_4_TEXT"	,"Q25"	,"Q25_5_TEXT"	,"Q26",	"Q1_NPS_GROUP",	"Q1"	,"Q2"	,"Q3_1",	"Q3_2"	,"Q3_3"	,"Q3_4",	"Q3_5"	,"Q4"	,"Q5_1"	,"Q5_2",	"Q5_4"	,"Q5_5"	,"Q5_6"	,"Q5_7"	,"Q5_8"	,"Q6"	,"Q7"	,"Q8"	,"Q14"	,"Q15_1"	,"Q11",	"Q12_1",	"Q13_1"	,"Q13_2"	,"Q13_3"	,"clientId"	,"scenarioName",	"scenarioOutcome"	,"scenarioStrategy1"	,"scenarioStrategy2"	,"scenarioStrategy3"	,"softwareVersion",	"preSimulationSurvey"	,"qLanguage"	,"webPortal"	,"hostName"	,"providerId"	,"userId",	"providerName",	"clientName"	,"projectName"	,"projectId"	,"mursionSessionId",	"scenarioId"
        );

        // Read existing file
        CSVReader reader = new CSVReader(new FileReader(inputFile));
        List<String[]> csvBody = reader.readAll();
        int i=0;
        String[] strArray = csvBody.get(i);
        for(int j=0;j<dtoHeaders.size();j++){
            csvBody.get(i)[j] = dtoHeaders.get(j);
        }

        reader.close();

        // Write to CSV file which is open
        CSVWriter writer = new CSVWriter(new FileWriter(inputFile));
        writer.writeAll(csvBody);
        writer.flush();
        writer.close();
    }

}
