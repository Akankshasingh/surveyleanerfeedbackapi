package com.test.demo.Controller.constants;

public enum SurveyType {
  SIM_FEEDBACK,
  PRE_SIMULATION,
  POST_SIMULATION
}
