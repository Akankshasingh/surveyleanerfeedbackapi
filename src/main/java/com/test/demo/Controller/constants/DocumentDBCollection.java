package com.test.demo.Controller.constants;

public enum DocumentDBCollection {

    SIMFeedbackCollection,
    PRESimulationCollection,
    POSTSimulationCollection

}
