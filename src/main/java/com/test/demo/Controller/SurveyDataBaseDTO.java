package com.test.demo.Controller;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class SurveyDataBaseDTO {

  private String providerId;
  private String providerName;
  private String userId;
  private String clientName;
  private String projectName;
  private String scenarioId;
  private String scenarioName;
  private String scenarioOutcome;
  private String projectId;
  private String mursionSessionId;

  private List<String> scenarioStrategy;
  private Boolean webPortal;
  private String surveyId;
  private String source;
  private Date startDate;
  private Date endDate;
  private String status;
  private String ipAddress;
  private Long progress;
  private Long durationSeconds;
  private boolean finished;
  private Date recordedDate;
  private String responseId;
  private String recipientLastName;
  private String recipientFirstName;
  private String recipientEmail;
  private String externalReference;
  private String locationLatitude;
  private String locationLongitude;
  private String distributionChannel;
  private String userLanguage;
  private List<String> questions;

}
