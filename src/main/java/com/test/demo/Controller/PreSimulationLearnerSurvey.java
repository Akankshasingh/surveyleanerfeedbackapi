package com.test.demo.Controller;

import lombok.Data;

import java.io.Serializable;

@Data
public class PreSimulationLearnerSurvey extends SurveyDataBaseDTO implements Serializable {

  private String sowId;

  private String joinSessionLink;

}
